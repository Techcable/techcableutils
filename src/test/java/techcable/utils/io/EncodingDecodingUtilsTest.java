package techcable.utils.io;

import static org.junit.Assert.*;
import static java.lang.System.out;

import java.util.Random;
import java.util.stream.IntStream;

import org.junit.Test;

import techcable.utils.StringUtils;

public class EncodingDecodingUtilsTest {
	
	@Test
	public void testVarints() {
		new Random().ints(1024).forEach((i) -> {
			byte[] bytes = EncodingUtils.encodeVarInt(i);
			assertEquals("unexpected varint", DecodingUtils.decodeVarInt(ByteInput.byteArrayInput(bytes)), i);
		});
	}
}
