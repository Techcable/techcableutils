package techcable.utils;

import java.util.UUID;

import org.apache.commons.lang3.RandomUtils;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;

public abstract class AbstractRNG implements RNG {
	@Override
	public abstract byte nextByte();
	@Override
	public byte[] nextBytes(int num) {
		byte[] bytes = new byte[num];
		for (int i = 0; i < num; i++) {
			bytes[i] = nextByte();
		}
		return bytes;
	}
	@Override
	public short nextShort() {
		return Shorts.fromByteArray(nextBytes(2));
	}
	@Override
	public int nextInt() {
		return Ints.fromByteArray(nextBytes(4));
	}
	@Override
	public long nextLong() {
		return Longs.fromByteArray(nextBytes(8));
	}
	@Override
	public float nextFloat() {
		return Float.intBitsToFloat(nextInt());
	}
	@Override
	public double nextDouble() {
		return Double.longBitsToDouble(nextLong());
	}
	@Override
	public String nextString() {
		return nextString(nextShort());
	}
	@Override
	public String nextString(int length) {
		return StringUtils.random(this, length);
	}
	@Override
	public UUID nextUUID() {
		return new UUID(nextLong(), nextLong());
	}
}
