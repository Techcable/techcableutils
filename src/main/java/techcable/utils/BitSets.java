package techcable.utils;

import java.util.BitSet;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.primitives.Ints;

public class BitSets {
	private static BitSet ALL_ON = ofByte((byte) 0xFF);
	private static BitSet NO_BITS = new BitSet(0);
	private BitSets() {}
	
	public static BitSet getAllOn() {
		return (BitSet) ALL_ON.clone();
	}
	
	public static BitSet getNoBits() {
		return (BitSet) NO_BITS.clone();
	}
	
	public static BitSet ofInt(int i) {
		return BitSet.valueOf(Ints.toByteArray(i));
	}
	
	public static BitSet ofByte(byte b) {
		return BitSet.valueOf(new byte[] {b});
	}
	
	public static int toInt(BitSet set) {
		byte[] bytes;
		byte[] rawBytes = set.toByteArray();
		if (rawBytes == null) {
			bytes = new byte[] {0x00, 0x00, 0x00, 0x00};
		}
		if (rawBytes.length >= 4) { 
			bytes = ArrayUtils.clone(rawBytes);
		} else if (rawBytes.length == 3) {
			bytes = new byte[] {rawBytes[0], rawBytes[1], rawBytes[2], 0x00};
		} else if (rawBytes.length == 2) {
			bytes = new byte[] {rawBytes[0], rawBytes[1], 0x00, 0x00};
		} else if (rawBytes.length == 1) {
			bytes = new byte[] {rawBytes[0], 0x00, 0x00, 0x00};
		} else {
			bytes = new byte[] {0x00, 0x00, 0x00, 0x00};
		}
		return Ints.fromByteArray(bytes);
	}
	
	public static byte toByte(BitSet set) {
		byte[] bytes = set.toByteArray();
		if (bytes == null || bytes.length == 0) {
			return 0x00;
		}
		return bytes[0];
	}
	
	
	
	public static BitSet[] toBytes(BitSet set) {
		byte[] bytes = set.toByteArray();
		BitSet[] sets = new BitSet[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			sets[i] = ofByte(bytes[i]);
		}
		return sets;
	}
}
