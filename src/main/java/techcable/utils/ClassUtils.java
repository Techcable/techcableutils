/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package techcable.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j;

import org.apache.commons.io.FileUtils;

/**
 *
 * @author Nicholas Schlabach
 */
@Log4j
public class ClassUtils {
	public static Class<?> getCallingClass() {
		try {
			return Class.forName(Thread.currentThread().getStackTrace()[2]
					.getClassName());
		} catch (ClassNotFoundException ex) {
			log.fatal(
					"This Should Never Happen unless The JVM does not know the Class of the calling Class",
					ex);
			throw new RuntimeException(
					"This Should Never Happen unless The JVM does not know the Class of the calling Class",
					ex);
		}
	}

	public static Class<?>[] getClases(File[] files, String[] classNames)
			throws IOException, ClassNotFoundException {
		URL[] urls = FileUtils.toURLs(files);

		URLClassLoader loader = URLClassLoader.newInstance(urls);
		List<Class<?>> classes = new ArrayList<>(classNames.length);

		for (String className : classNames) {
			classes.add(loader.loadClass(className));
		}

		return classes.toArray(new Class[0]);
	}

	public static Class<?> getClass(File file, String className)
			throws IOException, ClassNotFoundException {
		return getClases(new File[] { file }, new String[] { className })[0];
	}
}
