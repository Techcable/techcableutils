package techcable.utils;

import java.util.Random;
import java.util.UUID;

public interface RNG {
	public static final byte[] ONE_BYTE_ARRAY = new byte[1];
	public byte nextByte();
	public byte[] nextBytes(int num);
	public short nextShort();
	public int nextInt();
	public long nextLong();
	public float nextFloat();
	public double nextDouble();
	public String nextString(int length);
	public String nextString();
	public UUID nextUUID();
	public static RNG wrap(Random random) {
		return new AbstractRNG() {
			
			@Override
			public byte nextByte() {
				random.nextBytes(ONE_BYTE_ARRAY);
				return ONE_BYTE_ARRAY[0];
			}
		};
	}
	public static RNG create() {
		return wrap(new Random());
	}
}
