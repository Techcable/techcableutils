package techcable.utils.event;

public interface Cancelable {
	public boolean isCancelable();
	public void cancel();
	public boolean isCanceled();
}
