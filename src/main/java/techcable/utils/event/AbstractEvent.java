package techcable.utils.event;

import lombok.Getter;

public abstract class AbstractEvent implements Event {
	private boolean canceled;
	@Override
	public boolean isCancelable() {
		return false;
	}
	
	@Override
	public void cancel() {
		if (isCancelable()) {
			canceled = true;
		}
	}
	
	@Override
	public boolean isCanceled() {
		if (!isCancelable()) return false;
		else return canceled;
	}
}
