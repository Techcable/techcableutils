package techcable.utils.event;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;

@Log4j
@AllArgsConstructor
public class ListenerList<T extends Event> {
	private final Set<Listener<T>> listeners = new HashSet<>();
	private final boolean async;
	private Executor executor;
	
	public ListenerList(boolean async) {
		this(async, Executors.newSingleThreadExecutor());
	}
	public ListenerList() {
		this(false, null);
	}
	public void register(Listener<T> listener) {
		listeners.add(listener);
	}
	
	public boolean unRegister(Listener<T> listener) {
		return listeners.remove(listener);
	}
	
	public void post(T event) {
		
		for (Listener<T> listener : listeners) {
			try {
				listener.accept(event);
			} catch (Exception ex) {
				log.error(listener.getClass().getName() + " threw a " + ex.getClass().getName(), ex);
			}
		}
	}
}
