package techcable.utils.event;

public class AbstractCancelableEvent extends AbstractEvent {
	@Override
	public boolean isCancelable() {
		return true;
	}
}
