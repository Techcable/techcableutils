package techcable.utils.event;

import java.util.function.Consumer;

public interface Listener<T extends Event> extends Consumer<T> {
}
