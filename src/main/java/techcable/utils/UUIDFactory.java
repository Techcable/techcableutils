package techcable.utils;

import java.util.Set;
import java.util.UUID;

import techcable.utils.collect.WeakHashSet;

/**
 * Creates Uniqe for this factory uuids
 * @author Nicholas Schlabach
 *
 */
public class UUIDFactory {
	private final boolean isGuaranteedUnique;
	private final Set<UUID> created = new WeakHashSet<>();
	private final RNG random = RNG.create();
	/**
	 * 
	 * @param isGuaranteedUnique each uuid will not be equal to the others that i have created;
	 */
	public UUIDFactory(boolean isGuaranteedUnique) {
		this.isGuaranteedUnique = isGuaranteedUnique;
	}
	public UUIDFactory() {
		this(false);
	}

	public UUID next(boolean isGuaranteedUnique) {
		if (isGuaranteedUnique) {
			UUID temp = random.nextUUID();
			if (created.contains(temp)) {
				return next();
			} else return temp;
		} else {
			UUID temp = random.nextUUID();
			created.add(temp);
			return temp;
		}
	}
	
	public UUID next() {
		return next(isGuaranteedUnique);
	}
}
