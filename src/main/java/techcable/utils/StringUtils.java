package techcable.utils;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;



import java.util.stream.Stream.Builder;

import techcable.utils.collect.CollectionUtils;

import com.google.common.base.Charsets;

public class StringUtils {

	/**
	 * 
	 * @return UTF-8
	 * @deprecated use Charsets.UTF_*
	 */
	@Deprecated
	public static Charset getCharset() {
		return Charsets.UTF_8;
	}

	public static String fromBytes(byte[] bytes) {
		return new String(bytes, Charsets.UTF_8);
	}

	public static byte[] toBytes(String string) {
		return string.getBytes(Charsets.UTF_8);
	}

	public static boolean areEqualIgnoreCase(Iterable<String> one,
			Iterable<String> two) {
		return areEqualIgnoreCase(
				CollectionUtils.iteratorToList(one.iterator()),
				CollectionUtils.iteratorToList(two.iterator()));
	}

	public static boolean areEqualIgnoreCase(List<String> one, List<String> two) {
		if (one.size() != two.size())
			return false;
		return areEqualIgnoreCaseLenient(one, two);
	}

	public static boolean areEqualIgnoreCaseLenient(List<String> one,
			List<String> two) {
		for (int i = 0; i < Math.max(one.size(), two.size()); i++) {
			String string1 = null;
			boolean string1Exists = false;
			String string2 = null;
			boolean string2Exists = false;
			if (one.size() < i) {
				string1 = one.get(i);
				string1Exists = true;
			}
			if (two.size() < i) {
				string2 = two.get(i);
				string2Exists = true;
			}

			if (string1Exists && string2Exists)
				if (!string1.equalsIgnoreCase(string2))
					return false;
		}
		return true;
	}

	public static boolean areEqualIgnoreCaseLenient(Iterable<String> one,
			Iterable<String> two) {
		return areEqualIgnoreCaseLenient(
				CollectionUtils.iteratorToList(one.iterator()),
				CollectionUtils.iteratorToList(two.iterator()));
	}
	public static Stream<String> random(int num) {
		RNG random = RNG.create();
		Builder<String> builder = Stream.builder();
		for (int i = 0; i < num; i++) {
			builder.accept(random(random));
		}
		return builder.build();
	}
	public static Stream<String> random() {
		RNG random = RNG.create();
		return Stream.generate(() -> random(random));
	}
	public static String random(RNG random) {
		return random(random, random.nextShort());
	}
	public static String random(RNG rng, int length) {
		byte[] bytes = rng.nextBytes(length);
		return new String(bytes, Charsets.US_ASCII);
	}
}
