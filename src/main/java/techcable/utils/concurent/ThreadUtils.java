package techcable.utils.concurent;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class ThreadUtils {
	
	public static final Executor executor = Executors.newCachedThreadPool();
	
	public static void run(Runnable runnable) {
		executor.execute(runnable);
	}
	public static <T> ListenableFuture<T> run(Callable<T> callable) {
		SettableFuture<T> future = SettableFuture.<T> create();
		run(new Runnable() {
			
			@Override
			public void run() {
				try {
					future.set(callable.call());
				} catch (Exception ex) {
					future.setException(ex);
				}
			}
		});
		return future;
	}
	public static <X extends Exception> ListenableFuture<X> run(Runnable runnable, Class<? extends X> exClass){
		SettableFuture<X> exFuture = SettableFuture. <X> create();
		ThreadUtils.run(() -> {
			try {
				runnable.run();
				exFuture.set(null);
			} catch (RuntimeException ex) {
				if (exClass.isInstance(ex)) {
					exFuture.set((X) ex);
				} else throw ex;
			}
		});
		return exFuture;
	}
	public static void runSwing(Runnable runnable) {
		SwingUtilities.invokeLater(runnable);
	}
}
