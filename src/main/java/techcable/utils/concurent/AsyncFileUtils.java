package techcable.utils.concurent;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

import com.google.common.io.Files;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import techcable.utils.FileUtils;
import techcable.utils.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AsyncFileUtils {

	public static ListenableFuture<String> readString(File file) {
		return ThreadUtils.run(new Callable<String>() {

			@Override
			public String call() throws Exception {
				return FileUtils.readString(file);
			}
			
		});
	}

	public static ListenableFuture<byte[]> readBytes(File file) {
		return ThreadUtils.run(new Callable<byte[]>() {

			@Override
			public byte[] call() throws Exception {
				return FileUtils.readBytes(file);
			}
			
		});
	}

	public static void writeString(File file, String string) throws IOException {
		ThreadUtils.run(new Runnable() {
			@Override
			public void run() {
				try {
					FileUtils.writeString(file, string);
				} catch(IOException ex) {
					throw new RuntimeIOException(ex);
				}
			}
		});
	}

	public static void writeBytes(File file, byte[] bytes) throws RuntimeIOException {
			ThreadUtils.run(new Runnable() {
				@Override
				public void run() {
					try {
						FileUtils.writeBytes(file, bytes);
					} catch(IOException ex) {
						throw new RuntimeIOException(ex);
					}
				}
			});
	}

	public static String getExtension(File file) {
		return Files.getFileExtension(file.getPath());
	}
	
}
