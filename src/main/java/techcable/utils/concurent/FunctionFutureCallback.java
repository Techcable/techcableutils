/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package techcable.utils.concurent;

import java.util.function.Function;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

/**
 *
 * @author Nicholas Schlabach
 */
@RequiredArgsConstructor
public class FunctionFutureCallback<I, O> implements FutureCallback<I> {
	private final Function<I, O> function;
	@Getter
	private SettableFuture<O> out = SettableFuture.<O> create();

	@Override
	public void onSuccess(I result) {
		out.set(function.apply(result));
	}

	@Override
	public void onFailure(Throwable t) {
		out.setException(t);
	}

	public static <I, O> ListenableFuture<O> create(ListenableFuture<I> in,
			Function<I, O> function) {
		FunctionFutureCallback<I, O> call = new FunctionFutureCallback<I, O>(
				function);
		Futures.addCallback(in, call);
		return call.getOut();
	}
}
