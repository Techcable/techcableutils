/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package techcable.utils.concurent;

import java.util.concurrent.Future;
import java.util.function.Function;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import com.google.common.util.concurrent.JdkFutureAdapters;
import com.google.common.util.concurrent.ListenableFuture;

/**
 *
 * @author Nicholas Schlabach
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FutureUtils {
	public static <I, O> ListenableFuture<O> transform(
			ListenableFuture<I> inFuture, Function<I, O> function) {
		return FunctionFutureCallback.<I, O> create(inFuture, function);
	}

	public static <I, O> ListenableFuture<O> transform(Future<I> inFuture,
			Function<I, O> function) {
		return transform(toGoogleFuture(inFuture), function);
	}

	public static <I, O> ListenableFuture<O> transform(
			com.ning.http.client.ListenableFuture<I> inFuture,
			Function<I, O> function) {
		return transform(toGoogleFuture(inFuture), function);
	}

	public static <T> ListenableFuture<T> toGoogleFuture(Future<T> future) {
		return JdkFutureAdapters.listenInPoolThread(future);
	}

	public static <T> ListenableFuture<T> toGoogleFuture(
			com.ning.http.client.ListenableFuture<T> future) {
		return NingGoogleFutureAdapter.create(future);
	}
}
