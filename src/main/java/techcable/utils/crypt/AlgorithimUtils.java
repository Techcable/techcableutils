/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package techcable.utils.crypt;

import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 *
 * @author Nicholas Schlabach
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AlgorithimUtils {
	static {
		CipherUtils.addProviders();
	}
	// Algorithims
	public static final String AES = "AES/ECB/PKCS5Padding";
	public static final String AES_CFB8 = "AES/CFB8/NoPadding";

	// Utilities
	public static boolean isValid(String string) {
		if (string == null || string.isEmpty())
			return false;
		try {
			Cipher.getInstance(string);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
			return false;
		}
		return true;
	}
}
