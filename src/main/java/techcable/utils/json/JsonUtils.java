/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package techcable.utils.json;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 *
 * @author Nicholas Schlabach
 */
public class JsonUtils {

	static {
		adapterMap = new HashMap<>();
		gson = buildGson();
	}
	private static final Map<Class, Object> adapterMap;
	private static Gson gson;

	public static JsonElement toJson(String string) {
		return new JsonPrimitive(string);
	}

	public static <T> JsonElement toJson(T obj, Class<T> type) {
		return gson.toJsonTree(obj, type);
	}

	public static <T> T fromJson(JsonElement element, Class<T> type) {
		return gson.fromJson(element, type);
	}

	public static String toJson(JsonElement element) {
		return gson.toJson(element);
	}

	public static boolean registerTypeAdapter(Class type, Object adapter) {
		boolean result = registerTypeAdapterNoRebuild(type, adapter);

		if (result)
			gson = buildGson();

		return result;
	}

	private static boolean registerTypeAdapterNoRebuild(Class type,
			Object adapter) {
		if (!isValidAdapter(type, adapter))
			return false;

		adapterMap.put(type, adapter);

		return true;
	}

	private static boolean isValidAdapter(Class type, Object adapter) {
		try {
			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(type, adapter);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	private static void registerDefaultTypeAdapters() {
		registerTypeAdapterNoRebuild(LocalDate.class, new LocalDateSerializer());
		registerTypeAdapterNoRebuild(LocalTime.class, new LocalTimeSerializer());
	}

	private static Gson buildGson() {
		GsonBuilder builder = new GsonBuilder();
		registerDefaultTypeAdapters();
		for (Class<?> type : adapterMap.keySet()) {
			builder.registerTypeAdapter(type, adapterMap.get(type));
		}
		builder.enableComplexMapKeySerialization();
		builder.setPrettyPrinting();
		return builder.create();
	}
	
	public static final String YEAR_MONTH_YEAR_NAME = "year";
	public static final String YEAR_MONTH_MONTH_NAME = "month";
	
	public static JsonElement asJson(YearMonth yearMonth) {
		JsonObject yearMonthObj = new JsonObject();
		yearMonthObj.addProperty(YEAR_MONTH_MONTH_NAME, yearMonth.getMonthValue());
		yearMonthObj.addProperty(YEAR_MONTH_YEAR_NAME, yearMonth.getYear());
		return yearMonthObj;
	}
	public static YearMonth asYearMonth(JsonElement yearMonth) {
		JsonObject yearMonthObj = yearMonth.getAsJsonObject();
		int month = yearMonthObj.get(YEAR_MONTH_MONTH_NAME).getAsInt();
		int year = yearMonthObj.get(YEAR_MONTH_YEAR_NAME).getAsInt();
		return YearMonth.of(year, month);
	}
}
