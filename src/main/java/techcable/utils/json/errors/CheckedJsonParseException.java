package techcable.utils.json.errors;

import com.google.gson.JsonParseException;

public class CheckedJsonParseException extends Exception {
	private static final long serialVersionUID = -2567928285017313834L;
	public CheckedJsonParseException(JsonParseException cause) {
		super(cause);
	}
	
	@Override
	public JsonParseException getCause() {
		return (JsonParseException) super.getCause();
	}
}
