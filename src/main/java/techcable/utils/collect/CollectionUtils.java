package techcable.utils.collect;

/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 *
 * @author Nicholas Schlabach
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CollectionUtils {
	public static <T> T[] iteratorToArray(Iterator<T> iterator) {
		T[] array = null;
		while (iterator.hasNext()) {
			array = ArrayUtils.add(array, iterator.next());
		}
		return array;
	}

	public static <K, V> Map<V, K> invertMap(Map<K, V> map) {
		BiMap<K, V> biMap = HashBiMap.<K, V> create(map);
		return biMap.inverse();
	}

	public static <T> List<T> iteratorToList(Iterator<T> iterator) {
		List<T> list = new ArrayList<>();
		while (iterator.hasNext()) {
			list.add(iterator.next());
		}
		return list;
	}
	
	public static <T> Iterator<T> enumerationToIterator(Enumeration<T> enumeration) {
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				return enumeration.hasMoreElements();
			}

			@Override
			public T next() {
				return enumeration.nextElement();
			}
		};
	}
	
	public static <T> Iterable<T> enumerationToIterable(Enumeration<T> enumeration) {
		return new Iterable<T>() {

			@Override
			public Iterator<T> iterator() {
				return enumerationToIterator(enumeration);
			}
		};
	}

	public static <T> T[] iterableToArray(Iterable<T> iterable) {
		return iteratorToArray(iterable.iterator());
	}
}
