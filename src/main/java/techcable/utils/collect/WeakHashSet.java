package techcable.utils.collect;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public class WeakHashSet<E> extends AbstractSet<E> {
	private WeakHashMap<E, Object> map = new WeakHashMap<>();
	private static final Object PRESENT = new Object();
	
	@Override
	public Iterator<E> iterator() {
		return map.keySet().iterator();
	}
	@Override
	public int size() {
		return map.size();
	}
	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}
	@Override
	public boolean add(E e) {
		return map.put(e, PRESENT)==null;
	}
	@Override
	public boolean remove(Object obj) {
		return map.remove(obj)==PRESENT;
	}
	@Override
	public void clear() {
		map.clear();
	}
}
