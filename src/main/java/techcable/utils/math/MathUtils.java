/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package techcable.utils.math;

import org.apache.commons.lang3.ArrayUtils;
import org.bouncycastle.util.Arrays;

/**
 *
 * @author Nicholas Schlabach
 */
public class MathUtils {
	public static long roundUp(long num, long increment) {
		if (isMultipleOf(num, increment))
			return num;
		long quotent = num / increment;
		return (quotent + 1) * increment;
	}

	public static long roundDown(long num, long increment) {
		if (isMultipleOf(num, increment))
			return num;
		long quotent = num / increment;
		return (quotent - 1) * increment;
	}

	public static long roundClosest(long num, long increment) {
		long remainder = num % increment;
		if (remainder == 0)
			return num;
		long quotent = num / increment;

		long minIncrement = quotent * increment;
		long maxIncrement = (quotent + 1) * increment;

		long closerTo = closerTo(num, minIncrement, maxIncrement);

		if (closerTo == minIncrement)
			return minIncrement;
		else
			return maxIncrement;
	}
	
	public static int roundClosest(int num, int increment) {
		return (int) roundClosest((long) num, (long) increment);
	}

	public static boolean isMultipleOf(long num, long multiple) {
		return num % multiple == 0;
	}

	public static long closerTo(long num, long num1, long num2) {
		long num2Diference = Math.abs(num2 - num);
		long num1Diference = Math.abs(num1 - num);
		long closestDiference = Math.min(num1Diference, num2Diference);

		if (closestDiference == num1Diference)
			return num1;
		else
			return num2;
	}
	
	public static long[] between(long num1, long num2) {
		long[] longs = new long[0];
		long startNum = Math.min(num1, num2);
		long endNum = Math.max(num1, num2);
		for (long l = startNum; l <= endNum; l++) {
			longs = ArrayUtils.add(longs, l);
		}
		return longs;
	}
	public static int[] between(int num1, int num2) {
		return toIntArray(between((long) num1, (long) num2));
	}
	public static double addAll(double[] doubles) {
		double sum = 0;
		for (double num : doubles) {
			sum += num;
		}
		return sum;
	}
	public static int addAll(int[] ints) {
		int sum = 0;
		for (int num : ints) {
			sum += num;
		}
		return sum;
	}
	private static int[] toIntArray(long[] longs) {
		int[] ints = new int[0];
		for (long loong : longs) {
			ints = ArrayUtils.add(ints, (int) loong);
		}
		return ints;
	}
}
