package techcable.utils.io;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.base.Charsets;
import com.google.common.primitives.Bytes;

public class EncodingUtils {

	private EncodingUtils() {}
	public static byte[] encodeVarInt(int i) {
		List<Byte> bytes = new ArrayList<>();
		loop : while (true) {	
			if ((i & ~0x7F) == 0) {
				bytes.add((byte)i);
				break loop;
			}
			
			bytes.add((byte)(i & 0x7F | 0x80));
			i >>>= 7;
		}
		return Bytes.toArray(bytes);
	}
	public static int varIntSize(int i) {
		return encodeVarInt(i).length;
	}
}
