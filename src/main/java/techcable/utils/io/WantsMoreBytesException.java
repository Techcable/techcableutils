package techcable.utils.io;

import lombok.*;

public class WantsMoreBytesException extends RuntimeException {
    @Getter
    private final int bytesNeaded;
    
    public WantsMoreBytesException() {
	this(15);
    }

    public WantsMoreBytesException(int bytesNeaded) {
	this.bytesNeaded = bytesNeaded;
    }
}