package techcable.utils.io;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import com.google.common.base.Charsets;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;

public abstract class Input implements ByteInput{
	public static Input byteArrayInput(byte[] backing) {
		return byteBufInput(Unpooled.copiedBuffer(backing));
	}
	
	public static Input byteBufInput(ByteBuf backing) {
		return new Input() {
			
			@Override
			public byte readByte() {
				try {
					return backing.readByte();
				} catch (IndexOutOfBoundsException ex) {
					throw new WantsMoreBytesException();
				}
			}
			
			@Override
			public int available() {
				return  backing.readableBytes();
			}
		};
	}
	
	public synchronized byte[] readBytes(int num) {
		if (Integer.signum(num) == -1) throw new IllegalArgumentException("num is negative: " + num);
		if (num == 0) return new byte[0];
		byte[] bytes = new byte[num];
		try {
			for (int i = 0; i < num; i++) {
				bytes[i] = readByte();
			}
		} catch (WantsMoreBytesException ex) {
			throw new WantsMoreBytesException(num);
		}
		return bytes;
	}

	public synchronized short readShort() {
		return Shorts.fromByteArray(readBytes(2));
	}

	public synchronized int readInt() {
		return Ints.fromByteArray(readBytes(4));
	}

	public synchronized long readLong() {
		return Longs.fromByteArray(readBytes(8));
	}

	public synchronized float readFloat() {
		return Float.intBitsToFloat(readInt());
	}

	public synchronized double readDouble() {
		return Double.longBitsToDouble(readLong());
	}
	public synchronized int readVarInt() {
		return DecodingUtils.decodeVarInt(this);
	}

	public synchronized String readString() {
		int length = readVarInt();
		byte[] data = readBytes(length);
		String string = new String(data, Charsets.UTF_8);
		return string;
	}
}
