package techcable.utils.io;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public interface ByteInput {
	/**
	 * read a byte
	 * @return a byte from the input
	 * @throws WantsMoreBytesException if you are out of bytes
	 */
	public byte readByte() throws WantsMoreBytesException;
	/**
	 * get the number of bytes still available
	 * @return the number of bytes still available
	 */
	public int available();
	
	public static ByteInput byteArrayInput(byte[] backing) {
		return byteBufInput(Unpooled.copiedBuffer(backing));
	}
	
	public static ByteInput byteBufInput(ByteBuf backing) {
		return new ByteInput() {
			
			@Override
			public byte readByte() {
				try {
					return backing.readByte();
				} catch (IndexOutOfBoundsException ex) {
					throw new WantsMoreBytesException();
				}
			}
			
			@Override
			public int available() {
				return  backing.readableBytes();
			}
		};
	}
}
