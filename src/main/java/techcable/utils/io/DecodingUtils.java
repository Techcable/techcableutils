package techcable.utils.io;

import com.google.common.base.Charsets;

public class DecodingUtils {
	public static final int MAX_VARINT_SIZE = 5;
	private DecodingUtils() {}
	/**
	 * Copied from PacketLib
	 * @param input the ByteInput to read from
	 * @return a var int from the input
	 */
	public static int decodeVarInt(ByteInput input) {
		synchronized (input) {
			int out = 0;
			int bytes = 0;
			byte in;
			while (true) {
				in = input.readByte();
				
				out |= (in & 0x7F) << (bytes++ * 7);
				
				if (bytes > MAX_VARINT_SIZE) {
					throw new RuntimeException("Varint is too big");
				}
				
				if ((in & 0x80) != 0x80) {
					break;
				}
			}
			return out;
		}
	}
}
