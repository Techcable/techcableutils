package techcable.utils.io;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.util.BitSet;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;

public abstract class Output implements ByteOutput {
	public static Output byteArrayOutput(byte[] bytes) {
		return byteBufOutput(Unpooled.copiedBuffer(bytes));
	}
	public static Output byteBufOutput(ByteBuf buf) {
		return new Output() {
			@Override
			public void writeByte(byte b) {
				buf.writeByte(b);
			}
		};
	}
	public void writeBytes(byte[] bytes) {
		if (bytes == null || bytes.length == 0) return;
		for (byte b : bytes) {
			writeByte(b);
		}
	}
	
	public void writeShort(short s) {
		writeBytes(Shorts.toByteArray(s));
	}
	
	public void writeInt(int i) {
		writeBytes(Ints.toByteArray(i));
	}
	
	public void writeLong(long l) {
		writeBytes(Longs.toByteArray(l));
	}
	
	public void writeFloat(float f) {
		writeInt(Float.floatToIntBits(f));
	}
	
	public void writeDouble(double d) {
		writeLong(Double.doubleToLongBits(d));
	}
	
	public void writeString(String string) {
		byte[] bytes = string.getBytes(Charsets.UTF_8);
		writeVarInt(bytes.length);
		writeBytes(bytes);
	}
	
	public void writeVarInt(int i) {
		writeBytes(EncodingUtils.encodeVarInt(i));
	}
}
