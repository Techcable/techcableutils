package techcable.utils.io;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public interface ByteOutput {
	public void writeByte(byte b);
	public static ByteOutput byteArrayOutput(byte[] array) {
		return byteBufOutput(Unpooled.copiedBuffer(array));
	}
	public static ByteOutput byteBufOutput(ByteBuf buf) {
		return new Output() {
			@Override
			public void writeByte(byte b) {
				buf.writeByte(b);
			}
		};
	}
}
