package techcable.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.google.common.base.Throwables;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

@Log4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SerilizationUtils {
	
	private static boolean isSerializable(Class<?> clazz) {
		return Serializable.class.isAssignableFrom(clazz);
	}
	
	public static byte[] serialize(Serializable serializable) {
		return serialize((Object) serializable);
	}
	private static byte[] serialize(Object obj) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream objOut = new ObjectOutputStream(out);
			objOut.writeObject(obj);
			byte[] bytes = out.toByteArray();
			objOut.close();
			return bytes;
		} catch (Exception ex) {
			log.error("error durring serialization", ex);
			return null;
		}
	}
	
	public static Object deserialize(byte[] bytes) {
		try {
			ByteArrayInputStream in = new ByteArrayInputStream(bytes);
			ObjectInputStream objIn = new ObjectInputStream(in);
			Object obj = objIn.readObject();
			objIn.close();
			return obj;
		} catch (Exception ex) {
			log.error("error durring deserilization", ex);
			return null;
		}
	}
	public static <T> T deserialize(byte[] bytes, Class<T> clazz) {
		Object obj = deserialize(bytes);
		if (obj == null || !clazz.isInstance(obj)) {
			return null;
		}
		return (T) obj;
	}
}
