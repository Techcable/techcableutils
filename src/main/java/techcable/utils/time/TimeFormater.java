package techcable.utils.time;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class TimeFormater {

	public static String formatMilisecond(LocalTime time) {
		StringBuilder builder = new StringBuilder(3);
		int mili = time.get(ChronoField.MILLI_OF_SECOND);
		
		if (mili < 100) {
			builder.append(' ');
		}
		if (mili < 10) {
			builder.append(' ');
		}
		builder.append(mili);
		return builder.toString();
	}
	public static String formatMilisecond(LocalDateTime time) {
		return formatMilisecond(time.toLocalTime());
	}
	
	public static String formatSecond(LocalTime time) {
		StringBuilder builder = new StringBuilder(2);
		int sec = time.getSecond();
		if (sec < 10) {
			builder.append('0');
		}
		builder.append(sec);
		return builder.toString();
	}
	public static String formatSecond(LocalDateTime time) {
		return formatSecond(time.toLocalTime());
	}
	
	public static String formatMinute(LocalTime time) {
		StringBuilder builder = new StringBuilder(2);
		int min = time.getMinute();
		if (min < 10) {
			builder.append('0');
		}
		builder.append(min);
		return builder.toString();
	}
	public static String formatMinute(LocalDateTime time) {
		return formatMinute(time.toLocalTime());
	}
	public static String formatHour(LocalTime time) {
		return String.valueOf(time.get(ChronoField.HOUR_OF_AMPM) + 1);
	}
	public static String formatHour(LocalDateTime time) {
		return formatHour(time.toLocalTime());
	}
	public static String formatAmPm(LocalTime time) {
		int AM_PM = time.get(ChronoField.AMPM_OF_DAY);
		switch (AM_PM) {
		case 0 : return "AM";
		case 1 : return "PM";
		default : return String.valueOf(AM_PM);
		}
	}
	public static String formatAmPm(LocalDateTime time) {
		return formatAmPm(time.toLocalTime());
	}
}
