package techcable.utils.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoField;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class DateFormater {
	public static String formatEra(LocalDate date) {
		int era = date.get(ChronoField.ERA);
		switch (era) {
		case 1 : return "AD";
		case 0 : return "BC";
		}
		throw new IllegalArgumentException("Invalid Era Value");
	}
	public static String formatEra(LocalDateTime date) {
		return formatEra(date.toLocalDate());
	}
	
	public static String formatYear(LocalDate date) {
		return String.valueOf(date.get(ChronoField.YEAR_OF_ERA));
	}
	public static String formatYear(LocalDateTime date) {
		return formatYear(date.toLocalDate());
	}
	
	public static String formatMonth(LocalDate date) {
		Month month = date.getMonth();
		switch (month) {
		case JANUARY : return "January";
		case FEBRUARY : return "February";
		case MARCH : return "March";
		case APRIL : return "April";
		case MAY : return "May";
		case JUNE : return "June";
		case JULY : return "July";
		case AUGUST : return "August";
		case SEPTEMBER : return "September";
		case OCTOBER : return "October";
		case NOVEMBER : return "November";
		case DECEMBER : return "December";
		}
		throw new IllegalStateException();
	}
	public static String formatMonth(LocalDateTime date) {
		return formatMonth(date.toLocalDate());
	}
	
	public static String formatDay(LocalDate day) {
		return String.valueOf(day.getDayOfMonth());
	}
	public static String formatDay(LocalDateTime day) {
		return formatDay(day.toLocalDate());
	}
	public static String formatWeek(DayOfWeek week) {
		switch (week) {
		case MONDAY : return "Monday";
		case TUESDAY : return "Tuesday";
		case WEDNESDAY : return "Wednesday";
		case THURSDAY : return "Thursday";
		case FRIDAY : return "Friday";
		case SATURDAY : return "Saturday";
		case SUNDAY : return "Sunday";
		}
		throw new IllegalStateException();
	}
}
