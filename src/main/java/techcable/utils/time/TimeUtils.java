/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package techcable.utils.time;

import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.ChronoField;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import techcable.utils.json.LocalTimeSerializer;
import techcable.utils.math.RoundMode;

/**
 *
 * @author Nicholas Schlabach
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TimeUtils {
	private static DateTimeFormatter NoNanoFormatter = new DateTimeFormatterBuilder()
			.appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .toFormatter();

	public static LocalTime restrictMinites(int increment, LocalTime time,
			RoundMode mode) {
		int newMin = mode.round(time.getMinute() + 1, increment);
		if (newMin == 60) {
			return time.plusHours(1).withMinute(0);
		} else
			return time.withMinute(newMin - 1);
	}

	public static YearMonth getYearMonth(Year year, Month month) {
		return YearMonth.of(year.getValue(), month);
	}

	public static YearMonth getYearMonth(LocalDate date) {
		return YearMonth.of(date.getYear(), date.getMonth());
	}

	public static Year getYear(YearMonth yearMonth) {
		return Year.of(yearMonth.getYear());
	}

	public static Year getYear(LocalDate date) {
		return Year.of(date.getYear());
	}
	
	public static LocalDate getLocalDate(YearMonth month, int day) {
		return LocalDate.of(month.getYear(), month.getMonth(), day);
	}

	public static boolean isValidDay(YearMonth month, int day) {
		return day > 0 && day <= month.lengthOfMonth();
	}

	public static LocalTime restrictMinites(int increment, LocalTime time) {
		return restrictMinites(increment, time, RoundMode.ROUND_CLOSEST);
	}
	
	public static DayOfWeek getDayOfWeek(YearMonth month, int day) {
		return getLocalDate(month, day).getDayOfWeek();
	}
	
	public static String compactFormat(LocalDate date) {
		return DateTimeFormatter.ISO_LOCAL_DATE.format(date);
	}
	public static String compactFormat(LocalTime time) {
		return NoNanoFormatter.format(time);
	}
	public static String compactFormat(LocalDateTime date) {
		return compactFormat(date.toLocalDate()) + '-' + compactFormat(date.toLocalTime());
	}
}
