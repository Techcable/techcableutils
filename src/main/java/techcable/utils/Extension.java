/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package techcable.utils;

import java.io.File;

import lombok.Getter;

/**
 *
 * @author Nicholas Schlabach
 */
@Getter
public class Extension {

	public static final Extension JSON = new Extension("JSON");
	public static final Extension TXT = new Extension("TXT");

	public Extension(String extension) {
		this.extension = extension.toLowerCase();
	}

	public Extension(File file) {
		this(FileUtils.getExtension(file));
	}

	private final String extension;

	@Override
	public final int hashCode() {
		return extension.hashCode();
	}

	/**
	 * accepts either File, String, or Extension
	 * 
	 * @param obj
	 *            thing to check
	 * @return weather or not the "obj" is equal to this
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (obj instanceof String)
			return equals(new Extension((String) obj));
		if (obj instanceof File)
			return equals(new Extension((File) obj));
		if (obj instanceof Extension)
			return getExtension().equals(((Extension) obj).extension);
		return false;
	}
}
