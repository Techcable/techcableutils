package techcable.utils.os;

import java.io.File;

import javax.swing.LookAndFeel;

import org.apache.commons.lang3.SystemUtils;

import com.google.common.net.MediaType;

public interface OperatingSystem {
	/**
	 * Doesn't Necessarily Exist
	 * @param programName the program name
	 * @param user the User
	 * @return where the program should save its config files
	 */
	public File getProgramDirForUser(String programName, User user);
	/**
	 * Assume Current User
	 * Doesn't Necessarily Exist
	 * For Example : 
	 *    On Windows (Techcable/Techcable Utils) would turn into
	 *    $HOME + "AppData/Local/Techcable/Techcable Utils"
	 * @param programSubDir the programs directory
	 * @return where the program should save its config files
	 */
	public File getProgramDir(String programSubDir);
	public LookAndFeel getSystemLookAndFeel();
	public User getUser();
	/**
	 * Doesn't Necessarily Exist
	 * For Example : 
	 *    On Windows (Techcable/Techcable Utils) would turn into
	 *    $HOME + "AppData/Local/Techcable/Techcable Utils"
	 * @param programSubDir the programs directory
	 * @param user the user
	 * @return where the program should save its user's files by default
	 */
	public File getDefaultSaveDirForUser(User user);
	/**
	 * Doesn't Necessarily Exist
	 * Should Assume Current User
	 * @return where the program should save its user's files by default
	 */
	public File getDefaultSaveDir();

	
	public String getName();
	public String getVersion();

}
