package techcable.utils.os;

import java.io.File;

import org.apache.commons.lang3.SystemUtils;

public class OSUtils {
	public static final OperatingSystem OS = getOS();
	public static File getUserDir() {
		return OS.getUser().getHome();
	}
	
	public static File getProgramDir(String programName) {
		return OS.getProgramDir(programName);
	}
	private static OperatingSystem getOS() {
		if (SystemUtils.IS_OS_WINDOWS) {
			return new WindowsOS(SystemUtils.OS_NAME, SystemUtils.OS_VERSION);
		} else if (SystemUtils.IS_OS_MAC) {
			return new MacOS(SystemUtils.OS_NAME, SystemUtils.OS_VERSION);
		} else if (SystemUtils.IS_OS_UNIX) {
			return new UnixOS(SystemUtils.OS_NAME, SystemUtils.OS_VERSION);
		} else throw new UnsupportedOperationException("Unsupported Operating System!");
	}
}
