package techcable.utils.os;

import java.io.File;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import com.google.common.io.Files;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import techcable.utils.FileUtils;

@RequiredArgsConstructor
@Getter
public class MacOS extends AbstractOS {
	private final String name;
	private final String version;
	@Override
	public File getProgramDirForUser(String programSubDir, User user) {
		return new File(user.getHome(), "Library/Application Support/" + programSubDir);
	}

	@Override
	public File getDefaultSaveDirForUser(User user) {
		return new File(user.getHome() + "Documents");
	}
	
}
