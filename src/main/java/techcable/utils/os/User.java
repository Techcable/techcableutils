package techcable.utils.os;

import java.io.File;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class User {
	private final File home;
	private final File currentDir;
	private final OperatingSystem OS;
	
	/**
	 * WARNING : DEPENDENT ON OPERATING SYSTEM IMPLEMENTATION
	 * @param programName the program
	 * @return where this program should save it's junk
	 */
	public File getProgramDir(String programName) {
		return OS.getProgramDirForUser(programName, this);
	}
	
	public File getDefaultSaveDir() {
		return OS.getDefaultSaveDirForUser(this);
	}
}
