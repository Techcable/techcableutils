package techcable.utils.os;

import java.io.File;
import java.io.IOException;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import lombok.extern.log4j.Log4j;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

import com.google.common.base.Throwables;
import com.google.common.net.MediaType;

@Log4j
public abstract class AbstractOS implements OperatingSystem {
	@Override
	public LookAndFeel getSystemLookAndFeel() {
		try {
			String name = UIManager.getSystemLookAndFeelClassName();
			return (LookAndFeel)Class.forName(name).newInstance();
		} catch (Exception ex) {
			throw Throwables.propagate(ex);
		}
	}
	/**
	 * May Return Different Users Every Time
	 */
	@Override
	public User getUser() {
		return new User(SystemUtils.getUserHome(), SystemUtils.getUserDir(), this);
	}
	@Override
	public File getDefaultSaveDir() {
		return getDefaultSaveDirForUser(getUser());
	}
	
	@Override
	public File getProgramDir(String programSubDir) {
		return getProgramDirForUser(programSubDir, getUser());
	}
}
