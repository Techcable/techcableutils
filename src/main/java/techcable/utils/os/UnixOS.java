package techcable.utils.os;

import java.io.File;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class UnixOS extends AbstractOS {
	private final String name;
	private final String version;
	@Override
	public File getProgramDirForUser(String programName, User user) {
		return new File(user.getHome(), "." + programName);
	}
	@Override
	public File getDefaultSaveDirForUser(User user) {
		return new File(user.getHome(), "Documents");
	}
}
