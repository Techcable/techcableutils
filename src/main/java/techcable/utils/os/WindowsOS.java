package techcable.utils.os;

import java.io.File;
import java.io.IOException;

import com.google.common.base.Throwables;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class WindowsOS extends AbstractOS {
	private final String name;
	private final String version;
	@Override
	public File getProgramDirForUser(String programSubDir, User user) {
		return new File(user.getHome(), "AppData/Roaming/" + programSubDir);
	}

	@Override
	public File getDefaultSaveDirForUser(User user) {
		return new File(user.getHome(), "My Documents");
	}
}
