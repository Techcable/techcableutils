package techcable.utils.http;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper = true)
@Getter
public class PostRequest extends HttpRequest {
	private Content content;

	public PostRequest(URL url, Set<Header> headers, Content content) {
		super(url, headers, RequestType.POST);
		this.content = content;
	}

	public PostRequest(URL url, Content content) {
		this(url, new HashSet<Header>(), content);
	}

}
