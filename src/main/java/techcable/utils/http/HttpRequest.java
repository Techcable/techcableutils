package techcable.utils.http;

import java.net.URL;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
public abstract class HttpRequest {
	private final URL url;
	private final Set<Header> headers;
	private final RequestType requestType;
}
