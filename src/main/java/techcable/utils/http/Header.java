package techcable.utils.http;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import com.google.common.collect.Lists;
import com.ning.http.client.FluentCaseInsensitiveStringsMap;
import com.ning.http.client.simple.HeaderMap;

@Getter
public class Header {

	public Header(String key, String value) {
		this(key, Lists.newArrayList(value));
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;
		if (obj instanceof Header) {
			Header header = (Header) obj;
			if (!header.key.equalsIgnoreCase(key)) return false;
			if (header.values.size() != values.size()) return false;
			Iterator<String> thisValues = header.values.iterator();
			Iterator<String> otherValues = values.iterator();
			while (thisValues.hasNext() && otherValues.hasNext()) {
				String thisString = thisValues.next();
				String otherString = otherValues.next();
				if (!thisString.equalsIgnoreCase(otherString)) return false;
			}
			return true;
		} else return false;
		
	}
	public Header(String key, List<String> values) {
		this.key = key;
		this.values = values;
	}

	private final String key;
	private final List<String> values;

	public static FluentCaseInsensitiveStringsMap toStringsMap(Set<Header> headers) {
		FluentCaseInsensitiveStringsMap map = new FluentCaseInsensitiveStringsMap();
		headers.forEach((Header header) -> map.put(header.getKey(), header.getValues()));
		return map;
	}
	
	public static HeaderMap toMap(Set<Header> headers) {
		return new HeaderMap(toStringsMap(headers));
	}
	
	public static Set<Header> toSet(FluentCaseInsensitiveStringsMap map) {
		return toSet(new HeaderMap(map));
	}
	
	public static Set<Header> toSet(HeaderMap map) {
		Set<Header> headerSet = new HashSet<>();
		map.forEach((String key, List<String> values) -> headerSet.add(new Header(key, values)));
		return headerSet;
	}
}
