package techcable.utils.http;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class GetRequest extends HttpRequest {
	public GetRequest(URL url, Set<Header> headers) {
		super(url, headers, RequestType.GET);
	}

	public GetRequest(URL url) {
		this(url, new HashSet<Header>());
	}
}
