package techcable.utils.http;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import techcable.utils.StringUtils;
import techcable.utils.concurent.FutureUtils;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import com.google.common.util.concurrent.ListenableFuture;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;

@NoArgsConstructor
public class Client {
	private static AsyncHttpClient client = new AsyncHttpClient();
	public static HttpResponse get(GetRequest request) throws IOException {
		Request ningRequest = toNingRequest(request);
		Response ningResponse = getResponse(ningRequest);
		return toHttpResponse(ningResponse);
	}
	

	public static HttpResponse post(PostRequest request) throws IOException {
		Request ningRequest = toNingRequest(request);
		Response ningResponse = getResponse(ningRequest);
		return toHttpResponse(ningResponse);
	}
	
	private static Request toNingRequest(PostRequest request) {
		RequestBuilder builder = new RequestBuilder(request.getRequestType().name());
		builder.setHeaders(Header.toStringsMap(request.getHeaders()));
		builder.setHeader(HttpHeaders.ACCEPT_CHARSET, request.getContent().getSet().name());
		builder.setHeader(HttpHeaders.CONTENT_TYPE, request.getContent().getType().toString());
		builder.setContentLength(request.getContent().getLength());
		builder.setBody(request.getContent().getString());
		Request ningRequest = builder.build();
		return ningRequest;
	}
	
	private static Request toNingRequest(GetRequest request) {
		RequestBuilder builder = new RequestBuilder(request.getRequestType().name());
		builder.setHeaders(Header.toStringsMap(request.getHeaders()));
		builder.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
		builder.setUrl(request.getUrl().toString());
		Request ningRequest = builder.build();
		return ningRequest;
	}
	
	private static Response getResponse(Request ningRequest) throws IOException {
		Response ningResponse;
		try {
			ningResponse = client.executeRequest(ningRequest).get();
		} catch (ExecutionException ex) {
			Throwables.propagateIfInstanceOf(ex.getCause(), IOException.class);
			throw new IOException("Excecution Exception", ex);
		} catch (InterruptedException ex) {
			throw new IOException("Task Interupted");
		}
		return ningResponse;
	}
	
	private static HttpResponse toHttpResponse(Response ningResponse) throws IOException {
		MediaType type = MediaType.parse(ningResponse.getContentType());
		Charset set = null;
		String charsetHeader = ningResponse.getHeader(HttpHeaders.CONTENT_ENCODING);
		if (charsetHeader != null && Charset.isSupported(charsetHeader)) set = Charset.forName(charsetHeader);
		if (set == null) {
			try {
				if (type.charset().isPresent()) set = type.charset().get();
			} catch (Exception ex) {}
		}
		if (set == null) set = Charsets.ISO_8859_1;
		return new HttpResponse(new Content(type, ningResponse.getResponseBodyAsBytes(), set), Header.toSet(ningResponse.getHeaders()));
	}
}
