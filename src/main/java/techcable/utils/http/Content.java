package techcable.utils.http;

import java.nio.charset.Charset;

import com.google.common.base.Charsets;
import com.google.common.net.MediaType;

import lombok.*;

@Getter
@EqualsAndHashCode(exclude="set")
public class Content {
	private final MediaType type;
	private final String string;
	private final int length;
	private final Charset set;
	
	public Content(MediaType type, String string) {
		this(type, string, type.charset().or(Charsets.UTF_8));
	}
	
	public Content(MediaType type, byte[] bytes, Charset set) {
		this(type, new String(bytes, set), set);
	}
	
	public Content(MediaType type, String string, Charset set) {
		this.string = string;
		this.type = type;
		length = string.getBytes(set).length;
		this.set = set;
	}
	
	public byte[] asBytes() {
		return getString().getBytes(set);
	}
}
