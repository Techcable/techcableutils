package techcable.utils.http;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Set;

import lombok.Getter;

import com.google.common.base.Charsets;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import com.ning.http.client.Response;

@Getter
public class HttpResponse {
	private final Content content;
	private final Set<Header> headers;

	public HttpResponse(Content content, Set<Header> headers) {
		this.content = content;
		this.headers = headers;
	}
}
