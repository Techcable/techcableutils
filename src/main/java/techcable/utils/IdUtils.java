/*
 * The MIT License
 *
 * Copyright 2014 Nicholas Schlabach.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package techcable.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import techcable.utils.collect.CollectionUtils;

/**
 *
 * @author Nicholas Schlabach
 */
public class IdUtils<T> {

	private static final long startId = Long.MIN_VALUE;
	private Set<Long> avaliableIds;
	private Map<Long, T> idMap;

	public IdUtils() {
		avaliableIds = new HashSet<>();
		avaliableIds.add(startId);
		idMap = new WeakHashMap<>();
	}

	private long nextId() {
		long nextId = Long.MIN_VALUE;
		boolean nextIdSet = false;
		if (avaliableIds.isEmpty()) {
			loop: for (long next : idMap.keySet()) {
				if (!idMap.containsKey(next + 1)) {
					avaliableIds.add(next + 1);
					nextId = next + 1L;
					nextIdSet = true;
					break loop;
				}
			}
		} else {
			loop: for (long next : avaliableIds) {
				if (idMap.containsKey(next)) {
					avaliableIds.remove(next);
				} else {
					nextId = next;
					nextIdSet = true;
					break loop;
				}
			}
		}
		if (!nextIdSet) {
			throw new RuntimeException("Unable to Find Next ID!!!");
		}
		return nextId;
	}

	public void setItem(long id, T item) {
		idMap.put(id, item);
	}

	public void useId(T item) {
		long id = useId();
		setItem(id, item);
	}

	public long useId() {
		long id = nextId();
		idMap.put(id, null);
		avaliableIds.remove(id);
		return id;
	}

	public T getItem(long id) {
		return idMap.get(id);
	}

	public void remove(long id) {
		idMap.remove(id);
		avaliableIds.add(id);
	}

	public boolean remove(T item) {
		long id;
		try {
			id = getId(item);
		} catch (IdException ex) {
			return false;
		}
		remove(id);
		return true;
	}

	public long getId(T item) throws IdException {
		long id = Long.MIN_VALUE;

		if (!idMap.containsValue(item)) {
			throw new IdException("Could Not Find Item's Id!");
		}
		Map map = CollectionUtils.invertMap(idMap);
		if (map.containsKey(item)) {
			return (Long) map.get(item);
		}
		throw new IdException("Could Not Find Item's Id!");
	}

	public Collection<T> getItems() {
		return idMap.values();
	}

	public Set<Long> getIds() {
		return idMap.keySet();
	}

	public static class IdException extends Exception {

		public IdException() {
		}

		public IdException(String message) {
			super();
		}

		public IdException(String message, Throwable cause) {
			super(cause);
		}

		public IdException(Throwable cause) {
			super();
		}
	}
}
