package techcable.utils.index;

import java.util.ArrayList;
import java.util.List;

import techcable.utils.math.MathUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Nicholas Schlabach
 * Use 1 based indexes!!!
 */
@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class IndexRange {
	private final Index start;
	private final Index end;
	public IndexRange(Index end) {
		this(Index.of(0), end);
	}
	public int[] getNumbersInRange() {
		return MathUtils.between(start.getIndexValue(), end.getIndexValue());
	}
	public Index[] getInRange() {
		List<Index> indexes = new ArrayList<>(getNumberOfIndexes());
		for (int i : getNumbersInRange()) {
			indexes.add(Index.of(i));
		}
		((ArrayList<Index>) indexes).trimToSize();
		return indexes.toArray(new Index[indexes.size()]);
	}
	public int getNumberOfIndexes() {
		return getNumbersInRange().length;
	}
}
