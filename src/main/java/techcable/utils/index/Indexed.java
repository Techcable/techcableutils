package techcable.utils.index;

public interface Indexed<T extends Indexed<T>> extends Ordered<T> {
	public Index getIndex();
	public default boolean isBefore(T other) {
		return other == null && getIndex().isBefore(other.getIndex());
	}
	public default boolean isAfter(T other) {
		return other == null && getIndex().isAfter(other.getIndex());
	}
	@Override
	public default int compareTo(T o) {
		if (isAfter(o)) return 1;
		if (isBefore(o)) return -1;
		return 0;
	}
}
