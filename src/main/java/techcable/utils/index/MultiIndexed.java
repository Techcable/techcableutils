package techcable.utils.index;

public interface MultiIndexed<T extends MultiIndexed<T>> extends Ordered<T>, Mergeable<T> {
	public default Index getStart() {
		return getRange().getStart();
	}
	public default Index getEnd() {
		return getRange().getStart();
	}
	
	public IndexRange getRange();
	
	@Override
	public default boolean isBefore(T other) {
		return getStart().isBefore(other.getStart()) && getEnd().isBefore(other.getEnd());
	}
	@Override
	public default boolean isAfter(T other) {
		return getStart().isAfter(other.getStart()) && getEnd().isAfter(other.getEnd());
	}
}
