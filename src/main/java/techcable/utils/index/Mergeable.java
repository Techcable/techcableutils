package techcable.utils.index;

public interface Mergeable<T> {
	public T merge(T other) throws NotMergeableException;
	public boolean canMerge(T other);
	public default T mergeUnchecked(T other) {
		try {
			return merge(other);
		} catch (NotMergeableException ex) {
			throw new RuntimeNotMergeableException(ex);
		}
	}
	
	public static class RuntimeNotMergeableException extends RuntimeException {
		private static final long serialVersionUID = -2219127334798467426L;
		public RuntimeNotMergeableException(String message, NotMergeableException cause) {
	        super(message, cause);
	    }
		public RuntimeNotMergeableException(NotMergeableException cause) {
			super(cause);
		}
	    @Override
	    public NotMergeableException getCause() {
	    	return (NotMergeableException) super.getCause();
	    }
	}
	
	public static class NotMergeableException extends Exception {
		private static final long serialVersionUID = 8686535652201070733L;
		public NotMergeableException() {
	        super();
	    }
	    public NotMergeableException(String message) {
	        super(message);
	    }
	    public NotMergeableException(String message, Throwable cause) {
	        super(message, cause);
	    }
	    public NotMergeableException(Throwable cause) {
	        super(cause);
	    }
	    protected NotMergeableException(String message, Throwable cause,
	                        boolean enableSuppression,
	                        boolean writableStackTrace) {
	        super(message, cause, enableSuppression, writableStackTrace);
	    }
	}
}
