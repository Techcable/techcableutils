package techcable.utils.index;

public interface Ordered<T> extends Comparable<T> {
	public boolean isBefore(T other);
	public boolean isAfter(T other);
	@Override
	public default int compareTo(T other) {
		if (isBefore(other)) return -1;
		if (isAfter(other)) return 1;
		return 0;
	}
}
