package techcable.utils.index;

import org.bouncycastle.util.Arrays;

import com.google.common.base.Ticker;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import techcable.utils.math.MathUtils;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@EqualsAndHashCode
@RequiredArgsConstructor(access=AccessLevel.PRIVATE)
public class Index implements Indexed<Index> {
	private final int indexValue;
	private static final LoadingCache<Integer, Index> cache = CacheBuilder.newBuilder().weakValues().build(new CacheLoader<Integer, Index>() {

		@Override
		public Index load(Integer key) throws Exception {
			return new Index(key);
		}
		
	});
	@Override
	public Index getIndex() {
		return this;
	}
	@Override
	public int compareTo(Index o) {
		if (isBefore(o)) return -1;
		if (isAfter(o)) return 1;
		return 0;
 	}
	public boolean isInRange(IndexRange range) {
		return Arrays.contains(range.getNumbersInRange(), indexValue);
	}
	public Index add(int index) {
		return Index.of(getIndexValue() + index);
	}
	public static Index of(int index) {
		if (index < 0) throw new IllegalArgumentException("Index can not be less than 0");
		return cache.getUnchecked(index);
	}
}
