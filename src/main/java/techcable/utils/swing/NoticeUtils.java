package techcable.utils.swing;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class NoticeUtils {
	/**
	 * Does Not Have To Be Run On The Event Dispatch Thread :)
	 * @param ex error
	 * @param actionName the action you were trying to perform
	 * @param log the log to log this error to
	 */
    public static void error(Throwable ex, String actionName, Logger log) {
    	if (!SwingUtilities.isEventDispatchThread()) {
    		SwingUtilities.invokeLater(() -> error(ex, actionName, log));
    	} else {
    		JOptionPane.showMessageDialog(null, ex.getClass().getName() + ": " + ex.getMessage(), actionName + " Error", JOptionPane.ERROR_MESSAGE);
        	if (log != null) log.error(actionName + " Error", ex);
    	}
    }
    
    public static void error(String error, String actionName, Logger log) {
    	if (!SwingUtilities.isEventDispatchThread()) {
    		SwingUtilities.invokeLater(() -> error(error, actionName, log));
    	} else {
        	JOptionPane.showMessageDialog(null, error, actionName + " Error", JOptionPane.ERROR_MESSAGE);
        	if (log != null) log.error(actionName + " Error");
    	}

    }
    /**
     * use the one with a log
     * @param ex error
     * @param actionName action trying to perform
     */
    @Deprecated
    public static void error(Throwable ex, String actionName) {
    	error(ex, actionName, null);
    }
    /**
     * use the one with a log
     * @param error the error
     * @param actionName action trying to perform
     */
    @Deprecated
    public static void error(String error, String actionName) {
    	error(error, actionName, null);
    }
}
